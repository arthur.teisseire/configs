"Defaults
filetype off
set fileformat=unix
set nocompatible
set number
set nohlsearch

"indent
set expandtab
set smartindent
set autoindent
set tabstop=4
set shiftwidth=4
autocmd FileType clj setlocal shiftwidth=2 tabstop=2
autocmd FileType html setlocal shiftwidth=2 tabstop=2

"Color
syntax on
colorscheme PaperColor
set background=dark

"Headers
"autocmd bufnewfile *.c so ~/.vim/c_header.txt
"autocmd bufnewfile *.c exe "1," . 3 . "g/File Name : .*/s//File Name : " .expand("%")
"autocmd bufnewfile *.h so ~/.vim/c_header.txt
"autocmd bufnewfile *.h exe "1," . 3 . "g/File Name : .*/s//File Name : " .expand("%")

"Syntax
hi link cErrInParen NONE
hi link cErrInBracket NONE

"NerdTree
map <C-n> :NERDTreeToggle<CR>
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif

"Vundle
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
Plugin 'VundleVim/Vundle.vim'
Plugin 'Valloric/YouCompleteMe'
Plugin 'scrooloose/nerdcommenter'
Plugin 'aperezdc/vim-template'
Plugin 'scrooloose/nerdtree'
Plugin 'jiangmiao/auto-pairs'
Plugin 'ntpeters/vim-better-whitespace'
call vundle#end()
filetype plugin indent on

"Split
map <Tab> <C-W>w

"YouCompleteMe
let g:ycm_global_ycm_extra_conf = '~/.vim/.ycm_extra_conf.py'
let g:ycm_confirm_extra_conf = 0
let g:ycm_key_invoke_completion = '<C-Space>'
set completeopt-=preview
let g:ycm_semantic_triggers =  {
\   'c' : [],
\}

"Templates
map \tc :Template *.c<CR>
map \th :Template *.h<CR>
map \tt :Template tests_*.c<CR>
map \tu :TemplateHere test<CR>
map \tf :TemplateHere for<CR>
map \tw :TemplateHere while<CR>
map \ti :TemplateHere if<CR>
map \tr :TemplateHere return<CR>
map \tm :TemplateHere Makefile<CR>
let g:templates_directory = '~/.vim/templates'
let g:templates_no_builtin_templates = 1
let g:templates_user_variables = [
	\	['PROJECT_NAME', 'GetProjectName'],
	\	['TEST_NAME', 'GetTestName']
	\ ]

function! GetProjectName()
	return system('~/bin/find_project_name')
endfunction

function! GetTestName()
	let filename = expand("%:t:r:r:r")
	return substitute(filename, 'tests_', '', '')
endfunction

"AutoPairs
let AutoPairsFlyMode = 0
